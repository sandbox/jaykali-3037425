<?php

namespace Drupal\build_hooks_netlify\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides route responses for the mis_gatsby profile.
 */
class NetlifyController extends ControllerBase {

  /**
   * Capture the failure payload.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   A simple string and 200 response.
   */
  public function postBackFail(Request $request) {
    $response = new Response();

    $raw_response = $request->getContent();
    $json_response = [];

    if (!empty($raw_response)) {
      $json_response = json_decode($raw_response);

      // Write relevant deploy data to the database.
      if (!empty($json_response)) {
        db_insert('netlify_deploys')
          ->fields(
            [
              'request' => $raw_response,
              'created' => strtotime($json_response->created_at),
              'url' => $json_response->ssl_url,
              'name' => $json_response->name,
              'title' => $json_response->title,
              'status' => $json_response->state,
              'error_message' => isset($json_response->error_message) ? $json_response->error_message : NULL,
            ]
          )->execute();
      }
    }

    $response->setContent('Failure!');
    return $response;
  }

  /**
   * Capture the success payload.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   A simple string and 200 response.
   */
  public function postBackSuccess(Request $request) {
    $response = new Response();

    $raw_response = $request->getContent();
    $json_response = [];

    if (!empty($raw_response)) {
      $json_response = json_decode($raw_response);

      // Write relevant deploy data to the database.
      if (!empty($json_response)) {
        db_insert('netlify_deploys')
          ->fields(
            [
              'request' => $raw_response,
              'created' => strtotime($json_response->created_at),
              'url' => $json_response->ssl_url,
              'name' => $json_response->name,
              'title' => $json_response->title,
              'status' => $json_response->state,
              'error_message' => "",
            ]
          )->execute();
      }
    }

    $response->setContent('Success!');
    return $response;
  }

  /**
   * Sidebar deployments for Netlify.
   */
  public function sidebarDeploys() {
    $result = db_query('SELECT * FROM {netlify_deploys}
      ORDER BY created DESC');

    $rows = $result->fetchAll();

    $output = '';

    if (!empty($rows)) {
      foreach ($rows as $row) {
        $output .= '<tr><td>' . t(':title returned status <strong>":status"</strong> for URL: <a href=":url">:url</a> at %time.',
          [
            ':url' => $row->url,
            ':title' => $row->title,
            ':status' => $row->status,
            '%time' => date('m/d/Y h:i:sa', $row->created),
          ]) . '</td></tr>';
      }

      $build = [
        '#markup' => '<table>' . $output . '</table>',
      ];
    }
    else {
      $build = [
        '#markup' => '<p>No deployments found.</p>',
      ];
    }

    return $build;
  }

}
